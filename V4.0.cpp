#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXLEN 1000  // 输入算式的最大长度

int judge_num(char *buffer,int strLen);
int getStr(char line[], int nmax);
int calculator(char *buffer,int strLen);
void unit_test(void);

int main()
{
	unit_test();
	int strLen,k=-1,i,s=0,m=0,n;
    char buffer[MAXLEN+1];
    printf("******四则运算计算器******\n");
    printf("请输入数字和算法:\n例：12.12/12.2=\n    12.1*21\n\n"); 
    while(1)
    {
    	printf("你的输入为：");
		strLen= getStr(buffer, MAXLEN);
//		printf("string = %s\nlength = %d\n", buffer, strLen);
		n=judge_num(buffer,strLen);
//		printf("%c",buffer[strLen-1]);
		
		if(n==0)
		printf("输入错误！请重新输入！\n");
		else
		{
//			printf("OK\n");
			int sm=calculator(buffer,strLen);
		}
	}
    return 0;
}

int calculator(char *buffer,int strLen)     //四则运算函数 
{
	int i,k;
	float a=0,b=0;
	char c,str1[MAXLEN],str2[MAXLEN];
	for(i=0;i<MAXLEN;i++)
	{	
		str2[i]=' ';
		str1[i]=' ';
	}
	for(i=0;i<strLen;i++)
	if(buffer[i]=='+'||buffer[i]=='-'||buffer[i]=='/'||buffer[i]=='*')
	{
		k=i;
		c=buffer[i];
//		printf("%c",c);
	}
	
	for(i=0;i<k;i++)
	str1[i]=buffer[i];
//	printf("string = %s ", str1);
	if(buffer[strLen-1]=='=')
		for(i=k+1;i<strLen-1;i++)
		str2[i-k-1]=buffer[i];
	else
		for(i=k+1;i<strLen;i++)
		str2[i-k-1]=buffer[i];
//	printf("string = %s ", str2);
	a= atof(str1);
	b= atof(str2);
//	printf("%f %f",a,b);
	
	switch(c)
    {
	    case'+':
	        printf("输出为：%.2f\n",a+b);
	        return 1;
	        break;
	    case'-':
	        printf("输出为：%.2f\n",a-b);
	        return 1;
	        break;
	    case'*':
	        printf("输出为：%.2f\n",a*b);
	        return 1;
	        break;
	    case'/':
	        if (b!=0.0)         //分母不能为0
	        {
			   printf("输出为：%.2f\n",a/b);
	             return 1;}
	        else{
			
	            printf("分母不能为0\n");
	            return 0;}
	        break;
	    default:
	    //    printf("error\n");
	        break;
    }	
//    strcpy(str1, "");
//    strcpy(str2, "");
//memset(str1, 0, sizeof a);          //清空数组 
//memset(str2, 0, sizeof a);          //清空数组 	
//	for(i=0;i<MAXLEN;i++)
//	{	
//		str2[i]='0';
//		str1[i]='0';
//	}
}

int getStr(char *buffer, int maxLen)   
{
    char c;  // 读取到的一个字符
    int len = 0;  // 当前输入的字符串的长度

    // 一次读取一个字符，保存到buffer
    // 直到遇到换行符(\n)，或者长度超过maxLen时，停止读取
    while( (c=getchar()) != '\n' ){
        buffer[len++]=c;  // 将读取到的字符保存到buffer
        if(len>=maxLen){
            break;
        }
    }
    buffer[len]='\0';  // 读取结束，在末尾手动添加字符串结束标志
    fflush(stdin);  // 刷新输入缓冲区

    return len;
}

int judge_num(char *buffer,int strLen) //判断输入是否正确 
{
	int k=-1,s=0,m=0,i;
		for(i=0;i<strLen;i++)
		{
			if(buffer[i]=='+') k=i;
			if(buffer[i]=='-') k=i;
			if(buffer[i]=='*') k=i;
			if(buffer[i]=='/') k=i;	
		}
		if(k==-1)
		{
			return 0;
		}
		else
		{
//			printf("%d",k+1);
			for(i=0;i<k;i++)
			if(buffer[i]=='0'||buffer[i]=='1'||buffer[i]=='2'||buffer[i]=='3'||buffer[i]=='4'||buffer[i]=='5'||buffer[i]=='6'||buffer[i]=='7'||buffer[i]=='8'||buffer[i]=='9'||buffer[i]=='.')  s++;
//			printf("%d",s);
			if(s!=k) return 0; 

			if(buffer[strLen-1]=='=')
			{
				for(i=k+1;i<strLen-1;i++)
				if(buffer[i]=='0'||buffer[i]=='1'||buffer[i]=='2'||buffer[i]=='3'||buffer[i]=='4'||buffer[i]=='5'||buffer[i]=='6'||buffer[i]=='7'||buffer[i]=='8'||buffer[i]=='9'||buffer[i]=='.')  m++;
//				printf("%d",m);
				if(m!=(strLen-k-2)) return 0;
			}
			else
			{
				for(i=k+1;i<strLen;i++)
				if(buffer[i]=='0'||buffer[i]=='1'||buffer[i]=='2'||buffer[i]=='3'||buffer[i]=='4'||buffer[i]=='5'||buffer[i]=='6'||buffer[i]=='7'||buffer[i]=='8'||buffer[i]=='9'||buffer[i]=='.')  m++;
//				printf("%d",m);
				if(m!=(strLen-k-1)) return 0;				
			}		
		}	
}

void unit_test(void)
{
	printf("单元测试：\n");
	printf("模块1---judge_num\n");
	printf("正确输入：12.2/23= 8\n");
	char buffer[8]={'1','2','.','2','/','2','3','='};
	int strLen=8;
	int n1=judge_num(buffer,strLen);
	printf("模块输出为：%d\n",n1);	
	
	printf("错误输入：q2.a-b3= 8\n");
	char buffer1[8]={'q' , '2','.','a','-','b','3','='};
	int strLen1=8;
	int n2=judge_num(buffer1,strLen1);
	printf("模块输出为：%d\n",n2);
	
	if(n1!=0&&n2==0)	printf("模块1---judge_num功能正常\n");
	else printf("模块1---judge_num功能异常\n");
	
	printf("模块2---calculator\n");
	printf("正确输入：12.2-23= 8\n");
	char buffer2[8]={'1','2','.','2','-','2','3','='};
	int strLen2=8;
	int n3=calculator(buffer2,strLen2);
	printf("模块输出为：%d\n",n3);
	printf("错误输入：42.38/0 8\n");
	char buffer3[9]={'4','2','.','3','8','/','0'};
	int strLen3=7;
	int n4=calculator(buffer3,strLen3);
	printf("模块输出为：%d\n",n4);
	if(n3==1&&n4==0)	printf("模块2---calculator功能正常\n");
	else printf("模块2---calculator功能异常\n");

	
}



